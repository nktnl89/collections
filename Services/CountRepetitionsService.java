package Services;

import Interfaces.CountRepetitionsServiceInterface;
import Interfaces.ReadFileToStringArrayInterface;

import java.io.IOException;
import java.util.*;

public class CountRepetitionsService implements CountRepetitionsServiceInterface {
    @Override
    public void getCountRepetitions(String input) throws IOException {
        ReadFileToStringArrayInterface readFileToStringArrayService = new ReadFileToStringArrayService();
        ArrayList<String> words = readFileToStringArrayService.getStringFromFile(input);
        Map mapCountRepetitions;
        mapCountRepetitions = new HashMap<String,Integer>();
        for (String word :
                words) {
            if (word.length() > 0) {
                if (mapCountRepetitions.get(word) == null) {
                    mapCountRepetitions.put(word, 1);
                } else {
                    Integer count = (Integer) mapCountRepetitions.get(word);
                    mapCountRepetitions.put(word, ++count);
                }
            }
        }
        Set keys = mapCountRepetitions.keySet();
        for (Object key : keys) {
            System.out.println(key + " : " + mapCountRepetitions.get(key));
        }
    }

    @Override
    public void getCountRepetitionsAPI(String input) throws IOException {
        ReadFileToStringArrayInterface readFileToStringArrayService = new ReadFileToStringArrayService();
        List<String> words = readFileToStringArrayService.getStringFromFileAPI(input);

        Map<String, Integer> mapCountRepetitions = words.stream()
                .filter(word -> word.length() > 0)
                .collect(HashMap::new, (m, c) -> {
                    if (m.containsKey(c))
                        m.put(c, m.get(c) + 1);
                    else
                        m.put(c, 1);
                }, HashMap::putAll);
        mapCountRepetitions.keySet().stream()
                .forEach(key -> System.out.println(key + " : " + mapCountRepetitions.get(key)));
    }
}
