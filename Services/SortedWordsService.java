package Services;

import Interfaces.ReadFileToStringArrayInterface;
import Interfaces.SortedWordsServiceInterface;

import java.io.IOException;
import java.util.*;

public class SortedWordsService implements SortedWordsServiceInterface {

    @Override
    public void getSorted(String fileName) throws IOException {
        ReadFileToStringArrayInterface readFileToStringArrayService = new ReadFileToStringArrayService();
        ArrayList<String> wordsFromFile = readFileToStringArrayService.getStringFromFile(fileName);
        Map mapCountRepetitions;
        mapCountRepetitions = new HashMap<String, Integer>();
        for (String word :
                wordsFromFile) {
            if (word.length() > 0) {
                if (mapCountRepetitions.get(word) == null) {
                    mapCountRepetitions.put(word, 1);
                } else {
                    Integer count = (Integer) mapCountRepetitions.get(word);
                    mapCountRepetitions.put(word, ++count);
                }
            }
        }

        List<String> wordsByRepeating = new ArrayList<String>(mapCountRepetitions.keySet());
        Collections.sort(wordsByRepeating, new Comparator<String>() {
            public int compare(String o1, String o2) {
                return (int) mapCountRepetitions.get(o1) - (int) mapCountRepetitions.get(o2);
            }
        });

        for (Object key : wordsByRepeating) {
            System.out.println(key + " : " + mapCountRepetitions.get(key));
        }

    }

    @Override
    public void getSortedAPI(String fileName) throws IOException {
        ReadFileToStringArrayInterface readFileToStringArrayService = new ReadFileToStringArrayService();
        List<String> words = readFileToStringArrayService.getStringFromFileAPI(fileName);
        Map<String, Integer> map = words.stream()
                .filter(word -> word.length() > 0)
                .collect(HashMap::new, (m, c) -> {
                    if (m.containsKey(c))
                        m.put(c, m.get(c) + 1);
                    else
                        m.put(c, 1);
                }, HashMap::putAll);
        map.keySet().stream()
                .sorted(new Comparator<String>() {
                    @Override
                    public int compare(String o1, String o2) {
                        return map.get(o1) - map.get(o2);
                    }
                })
                .forEach(key -> System.out.println(key + " : " + map.get(key)));
    }
}
