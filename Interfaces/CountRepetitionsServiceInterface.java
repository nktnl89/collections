package Interfaces;

import java.io.FileNotFoundException;
import java.io.IOException;

public interface CountRepetitionsServiceInterface {
    public void getCountRepetitions(String input) throws IOException;
    public void getCountRepetitionsAPI(String input) throws IOException;
}
