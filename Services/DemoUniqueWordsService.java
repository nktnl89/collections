package Services;

import Interfaces.UniqueWordsServiceInterface;

import java.io.IOException;

public class DemoUniqueWordsService {
    public void startDemo() throws IOException {
        System.out.println("------------------------------Задание 2------------------------------");
        UniqueWordsServiceInterface uniqueWordsService = new UniqueWordsService();
        uniqueWordsService.getUniqueWords("text.txt");

        System.out.println("------------------------------Задание 2*-----------------------------");
        //UniqueWordsServiceAPIInterface uniqueWordsServiceAPI = new UniqueWordsServiceAPI();
        uniqueWordsService.getUniqueWordsAPI("text.txt");
    }
}
