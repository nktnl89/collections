package Services;

import Interfaces.ReadFileToStringArrayInterface;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ReadFileToStringArrayService implements ReadFileToStringArrayInterface {
    @Override
    public ArrayList<String> getStringFromFile(String fileName) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new FileReader(fileName));
        StringBuilder fileText = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            fileText.append(line.toLowerCase());
        }
        Pattern pattern = Pattern.compile("\\p{Punct}|\\s|$");
        ArrayList<String> words = new ArrayList<String>();
        Collections.addAll(words, pattern.split(fileText));

        return words;
    }

    @Override
    public List<String> getStringFromFileAPI(String fileName) throws IOException {
        Pattern pattern = Pattern.compile("\\p{Punct}|\\s|$");
        Object[] lines = Files.lines(Paths.get(fileName)).toArray();

        StringBuilder fileText = new StringBuilder();
        Arrays.stream(lines)
                .forEach(word -> fileText.append(word.toString().toLowerCase()));
        List<String> words = new ArrayList<>();
        Collections.addAll(words, pattern.split(fileText));

        return words;
    }
}
