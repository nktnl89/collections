import Services.DemoCountRepetitions;
import Services.DemoSortedWordsService;
import Services.DemoUniqueWordsService;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        DemoCountRepetitions demoCountRepetitions = new DemoCountRepetitions();
        demoCountRepetitions.startDemo();

        DemoUniqueWordsService demoUniqueWords = new DemoUniqueWordsService();
        demoUniqueWords.startDemo();

        DemoSortedWordsService demoSortedWords = new DemoSortedWordsService();
        demoSortedWords.startDemo();
    }
}
