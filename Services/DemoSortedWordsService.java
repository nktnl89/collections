package Services;

import Interfaces.SortedWordsServiceInterface;

import java.io.IOException;

public class DemoSortedWordsService {
    public void startDemo() throws IOException {
        System.out.println("------------------------------Задание 3------------------------------");
        SortedWordsServiceInterface sortedWords = new SortedWordsService();
        sortedWords.getSorted("text.txt");

        System.out.println("------------------------------Задание 3*-----------------------------");
        //SortedWordsServiceAPIInterface sortedWordsServiceAPI = new SortedWordsServiceAPI();
        sortedWords.getSortedAPI("text.txt");
    }
}
