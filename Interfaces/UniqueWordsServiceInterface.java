package Interfaces;

import java.io.IOException;

public interface UniqueWordsServiceInterface {
    public void getUniqueWords(String fileName) throws IOException;
    public void getUniqueWordsAPI(String fileName) throws IOException;
}
