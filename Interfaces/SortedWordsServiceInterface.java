package Interfaces;

import java.io.IOException;

public interface SortedWordsServiceInterface {
    public void getSorted(String fileName) throws IOException;
    public void getSortedAPI(String fileName) throws IOException;
}
