package Services;

import Interfaces.CountRepetitionsServiceInterface;

import java.io.IOException;

public class DemoCountRepetitions {
    public void startDemo() throws IOException {
        System.out.println("------------------------------Задание 1------------------------------");
        CountRepetitionsServiceInterface countRepetitionsService = new CountRepetitionsService();
        try {
            countRepetitionsService.getCountRepetitions("text.txt");
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("------------------------------Задание 1*-----------------------------");
       // CountRepetitionsServiceAPIInterface countRepetitionsServiceAPI = new CountRepetitionsServiceAPI();
        countRepetitionsService.getCountRepetitionsAPI("text.txt");
    }
}
