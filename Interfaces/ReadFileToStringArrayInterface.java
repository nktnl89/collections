package Interfaces;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public interface ReadFileToStringArrayInterface {
    public ArrayList<String> getStringFromFile(String fileName) throws IOException;
    public List<String> getStringFromFileAPI(String fileName) throws IOException;

}
