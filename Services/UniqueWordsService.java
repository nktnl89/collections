package Services;

import Interfaces.ReadFileToStringArrayInterface;
import Interfaces.UniqueWordsServiceInterface;

import java.io.IOException;
import java.util.*;

public class UniqueWordsService implements UniqueWordsServiceInterface {
    @Override
    public void getUniqueWords(String fileName) throws IOException {
        ReadFileToStringArrayInterface readFileToStringArrayService = new ReadFileToStringArrayService();
        ArrayList<String> words = readFileToStringArrayService.getStringFromFile(fileName);
        HashSet hashSet = new HashSet(words);
        Iterator<String> iterator = hashSet.iterator();
        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }
    }

    @Override
    public void getUniqueWordsAPI(String fileName) throws IOException {
        ReadFileToStringArrayInterface readFileToStringArrayService = new ReadFileToStringArrayService();
        List<String> words = readFileToStringArrayService.getStringFromFileAPI(fileName);
        Map<String, Integer> map = words.stream()
                .filter(word -> word.length() > 0)
                .collect(HashMap::new, (m, c) -> {
                    if (m.containsKey(c))
                        m.put(c, m.get(c) + 1);
                    else
                        m.put(c, 1);
                }, HashMap::putAll);
        map.keySet().stream()
                .filter(key -> map.get(key)==1)
                .forEach(key -> System.out.println(key + " : " + map.get(key)));

    }
}
